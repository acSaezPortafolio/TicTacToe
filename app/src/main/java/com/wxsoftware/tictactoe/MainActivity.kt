package com.wxsoftware.tictactoe

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import com.wxsoftware.tictactoe.model.Game
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var game: Game

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        game = Game(this)
    }

    fun clickButton(view: View) {
        val buttonSelected = view as ImageButton

        val cellId = when (buttonSelected.id) {
            R.id.button1 -> 1
            R.id.button2 -> 2
            R.id.button3 -> 3
            R.id.button4 -> 4
            R.id.button5 -> 5
            R.id.button6 -> 6
            R.id.button7 -> 7
            R.id.button8 -> 8
            R.id.button9 -> 9
            else -> 1
        }

        switchAuto.isEnabled = false
        game.play(cellId, buttonSelected, switchAuto.isChecked)
    }

    fun repeatClick(view: View) {
        game.resetGame()
        finish()
        startActivity(this.intent)
    }
}
