package com.wxsoftware.tictactoe.context

import android.app.Application

class AppContext: Application() {

    companion object {
        private var instance: AppContext? = null
        fun instance() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}