package com.wxsoftware.tictactoe.model

import android.app.Activity
import android.widget.ImageButton
import com.wxsoftware.tictactoe.context.AppContext
import com.wxsoftware.tictactoe.gameHandler.GameIA
import com.wxsoftware.tictactoe.gameHandler.PlayManager
import kotlinx.android.synthetic.main.activity_main.view.*
import org.jetbrains.anko.toast
import com.wxsoftware.tictactoe.R

class Game(val activity: Activity) {
    var activePlayer: Int = 1
    var player1 = ArrayList<Int>()
    var player2 = ArrayList<Int>()
    val playManager = PlayManager()
    val gameIA = GameIA()
    var cellAutoPlay = -1

    fun resetGame() {
        player1.clear()
        player2.clear()
        activePlayer = 1
    }

    fun play(cellId: Int, button: ImageButton, autoPlay: Boolean) {
        if (activePlayer == 1) {
            player1.add(cellId)
        } else {
            player2.add(cellId)
        }
        activePlayer = playManager.playGame(button, player1, player2, activePlayer)
        if (!checkWinner()) {
            if (autoPlay) {
                if (activePlayer == 2) {
                    cellAutoPlay = gameIA.autoPlay(player1, player2)
                    if (cellAutoPlay > 0) {
                        player2.add(cellAutoPlay)
                        activePlayer = playManager.playGame(getButton(cellAutoPlay), player1, player2, activePlayer)
                    }
                }
            }
        }
    }

    private fun checkWinner(): Boolean {
        if (playManager.winner(player1, player2)) {
           if (activePlayer == 2) {
               AppContext.instance()!!.toast("Player 1 wins")
           } else {
               AppContext.instance()!!.toast("Player 2 wins")
           }
            return true
        }
        return false
    }


    private fun getButton(cell: Int): ImageButton = when (cell) {
        1 -> activity.findViewById(R.id.button1)
        2 -> activity.findViewById(R.id.button2)
        3 -> activity.findViewById(R.id.button3)
        4 -> activity.findViewById(R.id.button4)
        5 -> activity.findViewById(R.id.button5)
        6 -> activity.findViewById(R.id.button6)
        7 -> activity.findViewById(R.id.button7)
        8 -> activity.findViewById(R.id.button8)
        9 -> activity.findViewById(R.id.button9)
        else -> activity.findViewById(R.id.button1)
    }
}