package com.wxsoftware.tictactoe.gameHandler

import android.widget.ImageButton
import com.wxsoftware.tictactoe.R
import com.wxsoftware.tictactoe.context.AppContext

class PlayManager {

    fun playGame(button: ImageButton, player1: ArrayList<Int>, player2: ArrayList<Int>, activePlayer: Int): Int {
        val ap = if (activePlayer == 1) {
            button.setImageResource(R.drawable.ic_cross)
            2
        } else {
            button.setImageResource(R.drawable.ic_circle)
            1
        }
        button.setBackgroundColor(AppContext.instance()!!.resources.getColor(R.color.colorPrimaryDark, null))
        return ap
    }

    fun winner(player1: ArrayList<Int>, player2: ArrayList<Int>): Boolean {
        var winner = false

        //row 1
        if (player1.contains(1) && player1.contains(2) && player1.contains(3)) winner = true
        if (player2.contains(1) && player2.contains(2) && player2.contains(3)) winner = true

        //row 2
        if (player1.contains(4) && player1.contains(5) && player1.contains(6)) winner = true
        if (player2.contains(4) && player2.contains(5) && player2.contains(6)) winner = true

        //row 3
        if (player1.contains(7) && player1.contains(8) && player1.contains(9)) winner = true
        if (player2.contains(7) && player2.contains(8) && player2.contains(9)) winner = true

        //column 1
        if (player1.contains(1) && player1.contains(4) && player1.contains(7)) winner = true
        if (player2.contains(1) && player2.contains(4) && player2.contains(7)) winner = true

        //column 2
        if (player1.contains(5) && player1.contains(2) && player1.contains(8)) winner = true
        if (player2.contains(5) && player2.contains(2) && player2.contains(8)) winner = true

        //column 3
        if (player1.contains(6) && player1.contains(9) && player1.contains(3)) winner = true
        if (player2.contains(6) && player2.contains(9) && player2.contains(3)) winner = true

        //diagonal 1
        if (player1.contains(1) && player1.contains(5) && player1.contains(9)) winner = true
        if (player2.contains(1) && player2.contains(5) && player2.contains(9)) winner = true

        //diagonal 2
        if (player1.contains(7) && player1.contains(5) && player1.contains(3)) winner = true
        if (player2.contains(7) && player2.contains(5) && player2.contains(3)) winner = true

        return winner
    }
}