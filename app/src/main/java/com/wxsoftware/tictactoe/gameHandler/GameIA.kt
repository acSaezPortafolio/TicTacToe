package com.wxsoftware.tictactoe.gameHandler

import java.util.*
import kotlin.collections.ArrayList

class GameIA {

    fun autoPlay(player1: ArrayList<Int>, player2: ArrayList<Int>): Int {
        val emptyCells = ArrayList<Int>()

        for (cellId in 1..9) {
            if (!(player1.contains(cellId) || player2.contains(cellId))) emptyCells.add(cellId)
        }

        val r = Random()
        var randIndex = 0
        if (emptyCells.isNotEmpty()) {
            randIndex = r.nextInt(emptyCells.size)
            return emptyCells[randIndex]
        }

        return -1
    }
}